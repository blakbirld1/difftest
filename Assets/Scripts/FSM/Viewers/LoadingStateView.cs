﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingStateView : AView
{
    [SerializeField] Image progressBar;


    private Action CallBack;

    public override void Enter()
    {
        base.Enter();
        StartCoroutine(WaitForLoadingBar(2));
    }

    IEnumerator WaitForLoadingBar(float seconds)
    {
        float currentTime = seconds;

        while (currentTime > 0)
        {
            currentTime -= Time.deltaTime;

            progressBar.fillAmount = (seconds - currentTime) / (seconds+ 0.3f);

            yield return null;
        }

        while(CallBack == null)
        {
            yield return null;
        }
        progressBar.fillAmount = 1;

        yield return new WaitForSeconds(0.1f);

        CallBack?.Invoke();

    }


    public void CompletLoad(Action goToLoadout)
    {
        CallBack = goToLoadout;
    }
}
