﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameStateView : AView
{
    public Action OnEnd;

    [SerializeField] StartGamePopup startGamePopup;
    [SerializeField] GamePopup gamePopup;
    [SerializeField] string resultPopup;

    private string missPopupName = "MissPopup";
    private LevelData currLevel;

    private int currStage = 0;
    private int playerHits;


    private int currImage;

    public override void Enter()
    {
        currImage = 0;
        playerHits = 0;
        currStage = 0;

        gamePopup.gameObject.SetActive(false);
        startGamePopup.gameObject.SetActive(true);

        base.Enter();
        startGamePopup.OnStart += OnStartGame;
        gamePopup.OnMiss += OnMiss;
        gamePopup.OnHit += OnHit;

        currLevel = LocalServer.Instance.GetLevelData();

        startGamePopup.Setup(currLevel.title, currLevel.images_count, currLevel.diffs_count);
   
    }

    public void NextImage()
    {
        currImage++;
        if(currImage >= currLevel.images_count)
        {
            ShowResultPopup();
        }
        else
        {
            gamePopup.ShowNextImage();
        }
    }

    public override void Exit()
    {
        StopAllCoroutines();
        base.Exit();
        startGamePopup.OnStart -= OnStartGame;
        gamePopup.OnMiss -= OnMiss;
        gamePopup.OnHit -= OnHit;

    }

    public void OnMiss()
    {
        PopupsManager.Instance.Hide();
        PopupsManager.Instance.ShowPopup(missPopupName);
    }
    private void OnHit(int number)
    {

        playerHits++;
        gamePopup.PlayerHit(0,number , currImage);
        if (playerHits - (currImage * currLevel.diffs_count) == currLevel.diffs_count)
        {
            NextImage();
        }
    }

    private void ShowResultPopup()
    {

        PopupsManager.Instance.Hide();
        ResultPopup resultP =  PopupsManager.Instance.ShowPopup<ResultPopup>(resultPopup,()=> {
             OnEnd?.Invoke();
        });

        resultP.Setup(currLevel.players_count - currStage, currStage + 1);

    }

    private void OnStartGame()
    {
        startGamePopup.gameObject.SetActive(false);
        gamePopup.gameObject.SetActive(true);
        gamePopup.Setup(currLevel.title, currLevel.images_count, currLevel.diffs_count);
    }

}
