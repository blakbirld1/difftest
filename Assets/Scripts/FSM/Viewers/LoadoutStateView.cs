﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadoutStateView : AView
{

    public Action OnStartLevel;

    [SerializeField] Button startButton;

    public int selectedLevel;

    public override void Enter()
    {
        base.Enter();
        startButton.onClick.AddListener(StartLevel);
    }

    public override void Exit()
    {
        base.Exit();
        startButton.onClick.RemoveListener(StartLevel);
    }

    public void SelectLevel(int level)
    {
        selectedLevel += level;
    }


    public void StartLevel()
    {
        Debug.Log("StartLevel");
        OnStartLevel?.Invoke();
    }

}
