﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class StateManager : MonoBehaviour
{
    public AState[] States;

    private Dictionary<State, AState> m_StateDict = new Dictionary<State, AState>();
    private State m_CurrentState = State.None;

    private void Awake()
    {

        m_StateDict.Clear();

        int _stateCount = Enum.GetNames(typeof(State)).Length;


        foreach (State _state in (State[])Enum.GetValues(typeof(State)))
        {
            if (_state == State.None)
                continue;

                Type instanceType = Type.GetType(_state + "State");
                AState list = Activator.CreateInstance(instanceType) as AState;
                m_StateDict.Add(_state, list);
                list.Init(this);
                AView _go = Resources.Load<AView>("UI/Views/" + _state + "View");
                AView viewer = Instantiate(_go, transform);
                viewer.m_State = m_StateDict[_state];
                m_StateDict[_state].m_View = viewer;
            viewer.gameObject.SetActive(false);
        }
    }

    private void Start()
    {
        SwitchState(State.Loading);
    }

    private void Update()
    {
        if (m_CurrentState != State.None)
            m_StateDict[m_CurrentState].Tick();
    }

    public AState FindState(State _stateName)
    {
        return m_StateDict.ContainsKey(_stateName) ? m_StateDict[_stateName] : null;
    }

    public void SwitchState(State _newState, bool waitForLoad = false)
    {
        if (_newState == m_CurrentState)
            return;
        AState _state = FindState(_newState);
        if (null == _state)
        {
            Debug.LogError("Can't find the state named " + _newState);
            return;
        }
        if (m_CurrentState != State.None)
        {
            if (!waitForLoad)
            {
                m_StateDict[m_CurrentState].Exit(_state);
            }

            var prevState = m_CurrentState;
            _state.Enter(m_StateDict[m_CurrentState], () =>
            {
                if (waitForLoad)
                {
                    m_StateDict[prevState].Exit(_state);
                }
            });
        }
        else
        {
            _state.Enter(null);
        }
        m_CurrentState = _newState;
    }

    public AView GetCurrentView()
    {
        if (m_CurrentState != State.None && m_StateDict.ContainsKey(m_CurrentState))
            return m_StateDict[m_CurrentState].m_View;
        return null;
    }
}

public enum State
{
    None,
    Loading,
    Loadout,
    Game
}