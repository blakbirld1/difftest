﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameState : AState
{
    public override State m_State => State.Game;
    private GameStateView gameStateView;


    public override void Enter(AState from, UnityAction onLoaded = null)
    {
        base.Enter(from, onLoaded);
        gameStateView = m_View as GameStateView;
        gameStateView.OnEnd += GoToGameOver;

    }

    public void GoToGameOver()
    {
        Debug.Log("GoToGameOver");
        gameStateView.OnEnd -= GoToGameOver;
        m_StateManager.SwitchState(State.Loadout);
    }

}
