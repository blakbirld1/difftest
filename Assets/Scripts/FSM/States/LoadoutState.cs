﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LoadoutState : AState
{
    public override State m_State => State.Loadout;


    public override void Enter(AState from, UnityAction onLoaded = null)
    {
        base.Enter(from, onLoaded);
        (m_View as LoadoutStateView).OnStartLevel += GoToGame;
    }

    public override void Exit(AState to)
    {
        base.Exit(to);
        (m_View as LoadoutStateView).OnStartLevel -= GoToGame;
    }


    public void GoToGame()
    {
        m_StateManager.SwitchState(State.Game);
    }

}
