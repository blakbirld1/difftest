﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class AState 
{
    public AudioClip Theme;

    public UnityEvent OnEnter;
    public UnityEvent OnLoaded = new UnityEvent();
    public UnityEvent OnExit;

    public virtual State m_State { get; }
    public virtual AView m_View { get; set; }
    protected StateManager m_StateManager;

    public void Init(StateManager manager)
    {
        m_StateManager = manager;
    }

    public virtual void Enter(AState from, UnityAction onLoaded = null)
    {
        OnEnter?.Invoke();
        var onLoadedBase = onLoaded;
        onLoaded = () =>
        {
            onLoadedBase?.Invoke();
            OnLoaded?.RemoveListener(onLoaded);
        };

        OnLoaded?.AddListener(onLoaded);

        if (null != m_View)
        { 
            m_View.gameObject.SetActive(true);
            m_View.Enter();
        }
    }

    public virtual void CompleteLoad()
    {
        OnLoaded?.Invoke();
    }

    public virtual void Exit(AState to)
    {
        OnExit?.Invoke();
        if (null != m_View)
        {
            m_View.Exit();
            m_View.gameObject.SetActive(false);
        }
    }

    public virtual void Tick()
    {
    }
}