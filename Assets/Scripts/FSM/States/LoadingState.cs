﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LoadingState : AState
{
    public override State m_State => State.Loading;


    public override void Enter(AState from, UnityAction onLoaded = null)
    {
        base.Enter(from, onLoaded);

        OnLoadComplete();


    }

    public void OnLoadComplete()
    {
        (m_View as LoadingStateView).CompletLoad(GoToLoadout);
    }

    public void GoToLoadout()
    {
        m_StateManager.SwitchState(State.Loadout);
    }
}
