﻿using UnityEngine;


[System.Serializable]
public class CustomAnimationInfo
{
    [SerializeField] private AnimationClip _clip;
    [SerializeField] private string trigger;

    private YieldInstruction _yieldInstruction;

    public YieldInstruction YieldInstruction
    {
        get
        {
            if (_yieldInstruction == null)
            {
                _yieldInstruction = new WaitForSeconds(Duration);
            }

            return _yieldInstruction;
        }
    }

    public string Trigger
    {
        get { return trigger; }
    }

    public float Duration
    {
        get { return _clip.length; }
    }
}
