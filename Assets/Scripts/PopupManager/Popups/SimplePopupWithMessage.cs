﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

#if USE_TMP
    using TMPro;
#endif


public class SimplePopupWithMessage : SimplePopup
{
#if USE_TMP
        [SerializeField] protected TextMeshProUGUI _message;
#else
    [SerializeField] protected Text _message;
#endif
    public void Initialize(UnityAction closeButtonHandler, string message)
    {
        _closeButton.onClick.AddListener(closeButtonHandler);
        if (!string.IsNullOrEmpty(message))
        {
            _message.text = message;
        }
    }
}
