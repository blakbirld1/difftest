﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


public class PopupsManager : Singleton<PopupsManager>
{
    [SerializeField] private Image _backdrop;
    [SerializeField] private Transform _container;

    private Queue<BasePopup> _pendingPopups;

    public void ShowSimplePopupWithMessage(UnityAction closeButtonHandler, string message)
    {
        SimplePopupWithMessage popupWithMessage =
            Instantiate(Resources.Load<SimplePopupWithMessage>("UI/Popups/SimplePopupWithMessage"));
        popupWithMessage.Initialize(closeButtonHandler, message);

        ShowPopup(popupWithMessage);
    }

    public void ShowDialogPopup(UnityAction closeButtonHandler, UnityAction confirmButtonHandler,
        string message)
    {
        DialogPopup popupWithMessage =
            Instantiate(Resources.Load<DialogPopup>("UI/Popups/DialogPopup"));
        popupWithMessage.Initialize(closeButtonHandler, confirmButtonHandler, message);

        ShowPopup(popupWithMessage);
    }

    public void ShowPopup(string name, UnityAction closeButtonHandler = null)
    {
        BasePopup popupWithMessage =
         Instantiate(Resources.Load<BasePopup>("UI/Popups/" + name));
        popupWithMessage.Initialize(closeButtonHandler);

        ShowPopup(popupWithMessage);
    }

    public T  ShowPopup<T>(string name, UnityAction closeButtonHandler = null)
    {
        BasePopup popupWithMessage =
         Instantiate(Resources.Load<BasePopup>("UI/Popups/" + name));
        popupWithMessage.Initialize(closeButtonHandler);

        ShowPopup(popupWithMessage);

        return popupWithMessage.GetComponent<T>();

    }

    private void OnPopupHidden()
    {
        BasePopup nextPopup = null;
        _pendingPopups.Dequeue();
        if (_pendingPopups.Count > 0)
        {
            nextPopup = _pendingPopups.Peek();
        }

        if (nextPopup)
        {
            nextPopup.gameObject.SetActive(true);
            ShowPopup(nextPopup, true);
        }
        else
        {
            SetBackdropEnabled(false);
        }
    }

    public void Hide()
    {
        if (_pendingPopups.Count <= 0)
        {
            return;
        }

        BasePopup popupToHide = _pendingPopups.Peek();
        popupToHide.Hide(OnPopupHidden);
    }

    private void ShowPopup(BasePopup popup, bool isFromQueue = false)
    {
        if (!isFromQueue)
        {
            _pendingPopups.Enqueue(popup);
        }

        if (_pendingPopups.Count > 1 && !isFromQueue)
        {
            popup.transform.SetParent(_container, false);
            popup.gameObject.SetActive(false);
            return;
        }

        SetBackdropEnabled(popup.RequiresBackdrop);
        popup.transform.SetParent(_container, false);
        popup.transform.localPosition = Vector3.zero;
        popup.Show();
    }

    private void SetBackdropEnabled(bool enabled)
    {
        _backdrop.enabled = enabled;
    }

    protected void Awake()
    {
        _pendingPopups = new Queue<BasePopup>();
    }
}
