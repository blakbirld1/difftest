﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimerPopup : SimplePopup
{
    [SerializeField] float timer;
    [SerializeField] float timerTick;
    [SerializeField] TextMeshProUGUI timerText;
    private float currTime;




    protected override void OnViewShown()
    {
        base.OnViewShown();
        StartCoroutine(TimerCorutine());
    }


    protected override void OnViewHidden()
    {
        base.OnViewHidden();
        StopAllCoroutines();
    }
    private IEnumerator TimerCorutine()
    {
        currTime = timer;
        for (int i = 0; i < timer; i++)
        {
            yield return new WaitForSeconds(timerTick);
            currTime -= timerTick;
            timerText.text = Format(currTime);
             
        }
        Hide();
    }


    private string Format(float _time)
    {
        float minutes = Mathf.Floor(_time / 60);
        float seconds = Mathf.RoundToInt(_time % 60);
        string minutesText = minutes.ToString();
        string secondsText = Mathf.RoundToInt(seconds).ToString();

        if (minutes < 10)
        {
            minutesText = "0" + minutes.ToString();
        }
        if (seconds < 10)
        {
            secondsText = "0" + Mathf.RoundToInt(seconds).ToString();
        }

        return minutesText + ":" + secondsText;
    }

}
