﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


public class DialogPopup : SimplePopupWithMessage
{
    [SerializeField] protected Button _confirmButton;

    public void Initialize(UnityAction closeButtonHandler, UnityAction confirmButtonHandler)
    {
        Initialize(closeButtonHandler, confirmButtonHandler, string.Empty);
    }

    public void Initialize(UnityAction closeButtonHandler, UnityAction confirmButtonHandler, string message)
    {
        base.Initialize(closeButtonHandler, message);
        _confirmButton.onClick.AddListener(confirmButtonHandler);
    }
}
