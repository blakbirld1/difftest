﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class BasePopup : BaseView
{
    [SerializeField] protected bool _requiresBackdrop;
    [SerializeField] protected Button _closeButton;

    public virtual void Initialize(UnityAction closeButtonHandler)
    {
        if (_closeButton != null && closeButtonHandler != null)
        { 
            _closeButton.onClick.AddListener(closeButtonHandler);
         }
    }

    public bool RequiresBackdrop
    {
        get { return _requiresBackdrop; }
    }

    protected override void OnViewHidden()
    {
        base.OnViewHidden();
        Destroy(gameObject);
    }
}
