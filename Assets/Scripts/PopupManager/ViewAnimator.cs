﻿using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class ViewAnimator : MonoBehaviour
{
    private Animator _animator;

    public bool IsAnimating { get; internal set; }

    public void SetTrigger(CustomAnimationInfo info, Action onAnimationComplete)
    {
        IsAnimating = true;
        _animator.SetTrigger(info.Trigger);
        if (onAnimationComplete != null)
        {
            StartCoroutine(wait(info.YieldInstruction, onAnimationComplete));
        }
    }

    private IEnumerator wait(YieldInstruction instruction, Action handler)
    {
        yield return instruction;
        handler();
    }

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

}
