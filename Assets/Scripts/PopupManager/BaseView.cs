﻿using System;
using UnityEngine;

    [RequireComponent(typeof(ViewAnimator))]
    public class BaseView : MonoBehaviour {
        [SerializeField] private CustomAnimationInfo _showInfo;
        [SerializeField] private CustomAnimationInfo _hideInfo;

        protected ViewAnimator _viewAnimator;

        public bool IsVisible { get; protected set; }

        public bool IsAnimating {
            get { return _viewAnimator.IsAnimating; }
        }

        public virtual void Show(Action handler = null) {
            _viewAnimator.SetTrigger(_showInfo, () => {
                OnViewShown();
                if (handler != null) {
                    Debug.Log("Hide");
                    handler();
                }
            });
        }

        public virtual void Hide(Action handler = null) {
        if (_viewAnimator != null)
        {
            Debug.Log("Hide");
            _viewAnimator.SetTrigger(_hideInfo, () =>
            {
                OnViewHidden();
                if (handler != null)
                {
                    Debug.Log("Hide hundler");
                    handler();
                }
            });
        }
        else
        {
            Debug.Log("Hide");
            handler?.Invoke();
        }
        }

        protected virtual void OnViewShown() {
            _viewAnimator.IsAnimating = false;
            IsVisible = true;
        }

        protected virtual void OnViewHidden() {
            _viewAnimator.IsAnimating = false;
            IsVisible = false;
        }

        protected virtual void Awake() {
            _viewAnimator = GetComponent<ViewAnimator>();
        }

        protected virtual void Start() {
            
        }
    }
