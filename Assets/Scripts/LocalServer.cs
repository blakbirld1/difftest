﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalServer : Singleton<LocalServer>
{
    //Simulate Server
    public LevelData GetLevelData()
    {
     var levelData = new LevelData()
        {
            diffs_count = 5,
            title = "MyEpisode",
            episode_id = 1,
            rounds_count = 2,
            images_count = 2
        };

        return levelData;

    }

 }
