﻿

public class ImageData
{
    public int id;
    public string title;
    public int image_exp;
    public int diff_exp;
    public int first_exp;
    public string[] releases;
   // public Diff [] diffs;
}

public class Diff
{
    public int id;
    public float x;
    public float y;
}