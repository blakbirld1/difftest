﻿using System.Collections.Generic;

public class LevelData 
{
    public int id;
    public string title;
    public int order;
    public int players_count;
    public int rounds_count;
    public int images_count;
    public int diffs_count;
    public int type;
    public List<string> releases;
    public int episode_id;
}

