﻿public class GameData 
{
    public int id;
    public string title;
    public int players_count;
    public int images_count;
    public int diffs_count;
    public int type;
    public float [] releases;
    public int episode_id;
}
