﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePopup : SimplePopup
{
    public Action OnMiss;
    public Action <int> OnHit;

    [SerializeField] Image leftImage;
    [SerializeField] Image rightImage;


    [SerializeField] Toggle[] togles;
    [SerializeField] ImagePanel imagePanel;
    [SerializeField] GameObject hint;


    public void PlayerHit(int player, int hit, int imageCount)
    {
        if(player == 0)
        {
            togles[hit].isOn = true;
        }
    }

    public void Miss()
    {
        OnMiss?.Invoke();
    }

    public void Hit( int hitNumber)
    {
        OnHit?.Invoke(hitNumber);
    }

    public void ShowNextImage()
    {
        imagePanel.ShowCompletImage();
    }

    public void ShowHint()
    {
        imagePanel.ShowHint();
    }

    public void Setup(string title, int images_count, int diffs_count)
    {
        imagePanel.ActivateDiffs(diffs_count);
        foreach (var item in togles)
        {
            item.gameObject.SetActive(false);
        }
        for (int i = 0; i < diffs_count; i++)
        {
            togles[i].gameObject.SetActive(true);
            togles[i].isOn = false;
        }
    }
}
