﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class ResultPlayer : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI nickname;
    [SerializeField] TextMeshProUGUI findDiff;
    [SerializeField] TextMeshProUGUI diffBonus;
    [SerializeField] TextMeshProUGUI pictureBonus;
    [SerializeField] TextMeshProUGUI firstBonus;
    [SerializeField] TextMeshProUGUI Total;
    [SerializeField] Image avatar;

    public void Init(ResulPlayerData resulPlayerData)
    {
        nickname.text = resulPlayerData.Nickname;
        findDiff.text = resulPlayerData.foundDiff.ToString();
        diffBonus.text = resulPlayerData.DiffBonus.ToString();
        pictureBonus.text = resulPlayerData.PictureBonus.ToString();
        firstBonus.text = resulPlayerData.FirstBonus.ToString();
        Total.text = resulPlayerData.Total.ToString();
    }

    public void Hide()
    {
        transform.DOLocalMoveX(transform.localPosition.x + 50,1);
        transform.DOScale(Vector3.zero,1);
    }
}

public class ResulPlayerData
{
    public string Nickname;
    public string Avatar;
    public int foundDiff;
    public int DiffBonus;
    public int PictureBonus;
    public int FirstBonus;
    public int Total;
}

