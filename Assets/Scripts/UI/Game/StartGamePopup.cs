﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StartGamePopup : SimplePopup
{
    public Action OnStart;

    [SerializeField] TextMeshProUGUI levelName;

    [SerializeField] TextMeshProUGUI picturesCountText;
    [SerializeField] GameObject[] picturesCount;

    [SerializeField] TextMeshProUGUI differencesCountText;
    [SerializeField] GameObject[] differencesCount;

    [SerializeField] Button ReadyButton;
 

    public void Setup(string levelname, int pictures, int diff )
    {
        levelName.text = levelname; 


        picturesCountText.text = pictures.ToString();
        for (int i = 0; i < picturesCount.Length; i++)
        {
            if (i < pictures)
                picturesCount[i].SetActive(true);
            else
                picturesCount[i].SetActive(false);
        }


        differencesCountText.text = pictures.ToString();
        for (int i = 0; i < differencesCount.Length; i++)
        {
            if (i < diff)
                differencesCount[i].SetActive(true);
            else
                differencesCount[i].SetActive(false);
        }

    }

    public void StartGame()
    {
        Debug.Log("StartGame");
        OnStart?.Invoke();
    }

}
