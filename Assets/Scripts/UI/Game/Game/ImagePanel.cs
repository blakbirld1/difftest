﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ImagePanel : MonoBehaviour
{
    [SerializeField] GameObject completPopup;
    [SerializeField] GameObject completArrow;
    [SerializeField] DiffButton[] diffsLeft;
    [SerializeField] DiffButton[] diffsRight;
    private int diffsCount;

    public void ShowCompletImage()
    {
        completPopup.SetActive(true);
        completPopup.transform.localScale = Vector3.zero;

        var seq = DOTween.Sequence();
        seq.Append(completPopup.transform.DOScale(Vector3.one, 0.5f));
        seq.SetDelay(0.1f);
        seq.Append(completArrow.transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), 0.3f));
        seq.SetDelay(0.1f);
        seq.Append(completArrow.transform.DOScale(new Vector3(1, 1, 1), 0.3f));
        seq.Append(completArrow.transform.DOScale(new Vector3(1.5f, 1.5f, 1.5f), 0.3f));
        seq.SetDelay(0.1f);
        seq.Append(completArrow.transform.DOScale(new Vector3(1, 1, 1), 0.3f));
        seq.AppendCallback(HideCompletPopup);

    }

    public void ActivateDiffs(int _diffCount)
    {
        diffsCount = _diffCount;
        for (int i = 0; i < diffsLeft.Length; i++)
        {
            if (i < _diffCount)
            {
                diffsLeft[i].gameObject.SetActive(true);
                diffsLeft[i].SetSprite(i);
                diffsLeft[i].OnFind += onFind;
            }
            else
                diffsLeft[i].gameObject.SetActive(false);

        }
        for (int i = 0; i < diffsRight.Length; i++)
        {
            if (i < _diffCount)
            {
                diffsRight[i].gameObject.SetActive(true);
                diffsRight[i].SetSprite(i);
                diffsRight[i].OnFind += onFind;
            }
            else
                diffsRight[i].gameObject.SetActive(false);

        }
    }

    public void ShowHint()
    {
        for (int i = 0; i < diffsLeft.Length; i++)
        {
            if(diffsLeft[i].isActiveAndEnabled && diffsLeft[i].GetFindSprite())
            {
                diffsLeft[i].ShowHint();
                diffsRight[i].ShowHint();
                return;
            }
        }
    }

    private void onFind(int Id)
    {
        diffsLeft[Id].ChangeSprite();
        diffsRight[Id].ChangeSprite();
    }


    private void HideCompletPopup()
    {
        completPopup.SetActive(false);
        ActivateDiffs(diffsCount);
    }

}
