﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiffButton : MonoBehaviour
{
    public Action<int> OnFind;

    [SerializeField] private Sprite[] Images;
    [SerializeField] private Sprite FindImage;
    [SerializeField] private GameObject hint;
    private Image image;
    private Button button;
    private int buttonID;


    public bool GetFindSprite()
    {
        if (image.sprite.name != FindImage.name && !hint.activeInHierarchy)
        {

            return true;
        }

        return false;
    }

    private void Awake()
    {
        image = GetComponent<Image>();
        button = GetComponent<Button>();
        button.onClick.AddListener(OnClick);
    }

    public void SetSprite(int id)
    {
        hint.SetActive(false);    
        button.enabled = true;
        buttonID = id;
        image.sprite = Images[id];
    }

    public void ChangeSprite()
    {
        hint.SetActive(false);
        image.sprite = FindImage;
        button.enabled = false;
    }

    private void OnClick()
    {
        OnFind?.Invoke(buttonID);
      
    }

    public void ShowHint()
    {
        hint.SetActive(true);
    }
}
