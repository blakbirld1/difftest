﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using UnityEngine.Events;
using System.Text;

public class ResultPopup : SimplePopup
{
    [SerializeField] TextMeshProUGUI levelNumberText;
    [SerializeField] TextMeshProUGUI timerText;
    [SerializeField] GameObject[] stars;
    [SerializeField] HorizontalLayoutGroup horizontalLayout;

    private GameStateView gameStateView;  


    public void Init(GameStateView _gameStateView)
    {
        gameStateView = _gameStateView;
    }

    public override void Show(Action handler = null)
    {
        base.Show(handler);
    }

    public void Setup( int playerCount, int currStage)
    {  
        StartCoroutine(WaitComplete(currStage));
    }

    public IEnumerator WaitComplete (int stage)
    {
        string text = timerText.text;
       
        int timer = 3;

        for (int i = 0; i < 4; i++)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat(text, stage, timer);
            timerText.text = sb.ToString();
            yield return new WaitForSeconds(1);
            timer--;
        }
        yield return new WaitForSeconds(0.2f);
        _closeButton.onClick.Invoke();
    }



    public override void Initialize(UnityAction closeButtonHandler)
    {

        if (_closeButton != null )
        {
            _closeButton.onClick.AddListener(Close);
        }
        base.Initialize(closeButtonHandler);
    }

    private void Close()
    {
        Hide();
    }
 

}
